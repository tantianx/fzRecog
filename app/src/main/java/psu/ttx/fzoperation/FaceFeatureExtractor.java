package psu.ttx.fzoperation;

import android.content.Context;

import org.opencv.core.Mat;

/**
 * Created by ttx on 4/19/17.
 */

public abstract class FaceFeatureExtractor {
    public abstract void init(Context context);
    public abstract Mat extractFeatures(String filename);
}
