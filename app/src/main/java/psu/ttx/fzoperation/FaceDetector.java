package psu.ttx.fzoperation;

import android.graphics.Bitmap;
import android.os.Environment;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;

/**
 * Created by ttx on 9/28/17.
 */

public class FaceDetector {
    private CascadeClassifier cc;
    private File sdcard = Environment.getExternalStorageDirectory();

    public FaceDetector(){
        cc = new CascadeClassifier(sdcard.getAbsolutePath() + "/frontalface_alt.xml");
    }

    public Mat[] detectFace(Bitmap bitmap) {
        Mat img = new Mat();
        Utils.bitmapToMat(bitmap, img);
        MatOfRect faces = new MatOfRect();
        cc.detectMultiScale(img, faces, 1.5, 3, 0, new Size(60, 60), new Size());
        Rect[] facesArray = faces.toArray();
        if (facesArray.length > 0) {
            Rect[] fzRects = faces.toArray();
            Mat[] fzResult = new Mat[facesArray.length];

            for (int i = 0; i < facesArray.length; i++) {
                fzResult[i] = new Mat(img, fzRects[i]);
            }

            return fzResult;
        }
        else {
            return null;
        }

    }
}
