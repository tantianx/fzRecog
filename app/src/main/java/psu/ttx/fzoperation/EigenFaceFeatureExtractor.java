package psu.ttx.fzoperation;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.opencv.core.CvType.CV_32F;

/**
 * Created by ttx on 4/19/17.
 */

public class EigenFaceFeatureExtractor extends FaceFeatureExtractor {
    private Mat mean;
    private Mat eigenVectors;
    private Context context;

    static String EIGEN_VECTOR_FILE = "eigenvector.mat";
    static String MEAN_FILE = "mean.mat";

    private Mat readInMatrix(String filename)
    {
        AssetManager assetManager = context.getAssets();
        Mat result = new Mat();
        try {
            InputStream matFile = assetManager.open(filename);
            BufferedReader matFileReader = new BufferedReader(new InputStreamReader(matFile));
            String data;

            data = matFileReader.readLine();
            int row = Integer.parseInt(data);
            data = matFileReader.readLine();
            int column = Integer.parseInt(data);

            result = new Mat(row, column, CV_32F);

            double element;

            for (int i = 0; i < row; i++) {
                for (int j = 0; j < column; j++) {
                    data = matFileReader.readLine();
                    if (data.equals("nan") || data.equals("-nan") || data.length() == 0) {
                        element = 0;
                    }
                    else {
                        element = Double.parseDouble(data);
                    }
                    result.put(i, j, element);
                }
            }

        } catch (IOException e) {
            Log.v("CV", "Read matrix error");
            e.printStackTrace();
        }
        Log.v("CV", "Load file: " + filename);
        return result;
    }

    @Override
    public void init(Context context) {
        this.context = context;
        this.eigenVectors = readInMatrix(EIGEN_VECTOR_FILE);
        this.mean = readInMatrix(MEAN_FILE);
    }

    @Override
    public Mat extractFeatures(String filename) {
        File file = new File(filename);
        if(file.exists()) {
            Log.v("CV", "File exists");
        }

        Log.v("EIGEN", "START");
        Mat img = Imgcodecs.imread(filename);
        img.convertTo(img, CV_32F);
        Mat resizedImg = new Mat(img.size(), CV_32F);
        Imgproc.resize(img, resizedImg, new Size(50, 50));
        Imgproc.cvtColor(resizedImg, resizedImg, Imgproc.COLOR_BGR2GRAY);
        Imgproc.resize(resizedImg, resizedImg, new Size(1, 2500));

        Core.subtract(resizedImg, this.mean, resizedImg);
        Mat features = new Mat(resizedImg.size(), CV_32F);
        Core.gemm(this.eigenVectors, resizedImg, 1, new Mat(), 0, features);
        Log.v("EIGEN", "COMPLETE");
        return features;
    }
}
