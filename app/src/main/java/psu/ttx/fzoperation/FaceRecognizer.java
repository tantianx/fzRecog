package psu.ttx.fzoperation;

import android.graphics.Bitmap;
import android.os.Environment;

import com.sh1r0.caffe_android_lib.CaffeMobile;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.File;

import static android.graphics.Bitmap.createBitmap;
import static org.opencv.core.Core.BORDER_CONSTANT;
import static org.opencv.core.CvType.CV_32F;

/**
 * Created by ttx on 9/28/17.
 */

public class FaceRecognizer {
    public native Double[] alignFaceFromJNI(int[] pixels, int width, int height);
    public native void initFromJNI();

    File sdcard = Environment.getExternalStorageDirectory();
    String lightDir = sdcard.getAbsolutePath();
    String lightProto = lightDir + "/LightenedCNN_C_deploy.prototxt";
    String lightModelBinary = lightDir + "/LightenedCNN_C.caffemodel";

    private CaffeMobile caffeMobile = new CaffeMobile();

    public FaceRecognizer() {
        this.caffeMobile.loadModel(lightProto, lightModelBinary);
    }

    public float[][] alignAndExtractFeature(Mat image) {
        // Align face.
        Bitmap bitmap = createBitmap(image.cols(), image.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(image, bitmap);
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int[] pixels = new int[width * height];
        bitmap.getPixels(pixels, 0, width, 0, 0, width, height);
        Double[] alignResult = alignFaceFromJNI(pixels, width, height);

        if (alignResult != null) {
            int pad = Math.max(image.rows(), image.cols());
            Point center = new Point(alignResult[2] + pad, alignResult[3] + pad);
            Core.copyMakeBorder(image, image, pad, pad, pad, pad, BORDER_CONSTANT,
                    new Scalar(255, 255, 255));

            Mat rotation = Imgproc.getRotationMatrix2D(center,
                    alignResult[1], 1);
            Imgproc.warpAffine(image, image, rotation, new Size(image.rows(), image.cols()));

            Rect roi = new Rect((int) (center.x - alignResult[4]),
                    (int) (center.y - alignResult[5]),
                    (int) (alignResult[4] * 3), (int) (alignResult[5] * 3));
            image = new Mat(image, roi);
        }
        Imgproc.resize(image, image, new Size(128, 128));
        Imgproc.cvtColor(image, image, Imgproc.COLOR_RGB2GRAY);

        // Extract features.
        byte[] inputArray = new byte[width * height];
        image.put(height, width, inputArray);
        float [][] features = caffeMobile.extractFeatures(inputArray, width, height, "fc2");

        return features;
    }

    public float computeDistanceOfFeatures(float[][] f1, float[][] f2) {
        float result = 0;
        for (int i = 0; i < f1.length; i++) {
            for (int j = 0; j < f1[i].length; j++) {
                result += Math.pow(f1[i][j] - f2[i][j], 2);
            }
        }
        return result;
    }
}
