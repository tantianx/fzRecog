package psu.ttx.fzoperation;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.sh1r0.caffe_android_lib.CaffeMobile;
import org.opencv.core.Mat;

import java.io.File;

import static org.opencv.core.CvType.CV_32F;

/**
 * Created by ttx on 4/19/17.
 */

public class VGGFaceFeatureExtractor {
    private CaffeMobile caffeMobile;
    private Context context;

    File sdcard = Environment.getExternalStorageDirectory();

    String vggDir = sdcard.getAbsolutePath() + "/caffe_mobile/vgg_face_caffe";
    String vggProto = vggDir + "/VGG_FACE_deploy.prototxt";
    String vggModelBinary = vggDir + "/VGG_FACE.caffemodel";

    String lightDir = sdcard.getAbsolutePath();
    String lightProto = lightDir + "/LightenedCNN_C_deploy.prototxt";
    String lightModelBinary = lightDir + "/LightenedCNN_C.caffemodel";

    String lightProto2 = lightDir + "/LightenedCNN_A_deploy.prototxt";
    String lightModelBinary2 = lightDir + "/LightenedCNN_A.caffemodel";

    public void init(int model, Context context) {
        this.caffeMobile = new CaffeMobile();
        this.context = context;

//        caffeMobile.loadModel(vggProto, vggModelBinary);
        if (model == 1) {
            caffeMobile.loadModel(lightProto, lightModelBinary);
        }
        else {
            caffeMobile.loadModel(lightProto, lightModelBinary);
        }

        caffeMobile.setNumThreads(1);
    }

    public Mat extractFeatures(String filename) {
        Log.v("VGG", "START");
        float [][] features = caffeMobile.extractFeatures(filename, "fc2");
        Mat result = new Mat(features.length, features[0].length, CV_32F);
        for (int i = 0; i < features.length; i++) {
            for (int j = 0; j < features[i].length; j++) {
                result.put(i, j, features[i][j]);
            }
        }

        Log.v("VGG", "COMPLETE");
        return result;
    }
}
