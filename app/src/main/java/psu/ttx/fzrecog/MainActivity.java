package psu.ttx.fzrecog;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;

import java.io.File;

import psu.ttx.fzoperation.FaceRecognizer;
import psu.ttx.fzoperation.FaceDetector;

public class MainActivity extends AppCompatActivity {
    FaceDetector faceDetector = new FaceDetector();
    FaceRecognizer faceRecognizer = new FaceRecognizer();

    // Load caffe library.
    static {
        if (!OpenCVLoader.initDebug()) {
            Log.d("LIB", "Cannot init opencv.");
        }
        System.loadLibrary("opencv_java3");
        System.loadLibrary("caffe");
        System.loadLibrary("caffe_jni");
        System.loadLibrary("align_fz");
        Log.d("LIB", "Load all lib successfully.");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Button detectBtn = (Button) findViewById(R.id.detect_btn);
        detectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File sdcard = Environment.getExternalStorageDirectory();
                Bitmap bitmap = BitmapFactory.decodeFile(sdcard.getAbsolutePath() + "/test-photo.jpg");
                Mat[] faces = faceDetector.detectFace(bitmap);

                for (int i = 0; i < faces.length; i++) {
                    float[][] feature = faceRecognizer.alignAndExtractFeature(faces[i]);
                }
            }
        });
    }

    private BaseLoaderCallback loaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.d("LIB", "opencv loaded successfully.");
                }
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };
}
