//
// Created by Tianxiang Tan on 6/6/17.
//

#include <cstring>
#include <string>
#include <jni.h>
#include <vector>
#include <iostream>
#include <math.h>

#include "dlib/dlib/image_io.h"
#include "dlib/dlib/image_processing.h"
#include "dlib/dlib/image_processing/frontal_face_detector.h"
#include "dlib/dlib/image_processing/render_face_detections.h"
#include "dlib/dlib/dnn.h"

using namespace dlib;
using namespace std;

template <template <int,template<typename>class,int,typename> class block, int N, template<typename>class BN, typename SUBNET>
using residual = add_prev1<block<N,BN,1,tag1<SUBNET>>>;

template <template <int,template<typename>class,int,typename> class block, int N, template<typename>class BN, typename SUBNET>
using residual_down = add_prev2<avg_pool<2,2,2,2,skip1<tag2<block<N,BN,2,tag1<SUBNET>>>>>>;

template <int N, template <typename> class BN, int stride, typename SUBNET>
using block  = BN<con<N,3,3,1,1,relu<BN<con<N,3,3,stride,stride,SUBNET>>>>>;

template <int N, typename SUBNET> using ares      = relu<residual<block,N,affine,SUBNET>>;
template <int N, typename SUBNET> using ares_down = relu<residual_down<block,N,affine,SUBNET>>;

template <typename SUBNET> using alevel0 = ares_down<256,SUBNET>;
template <typename SUBNET> using alevel1 = ares<256,ares<256,ares_down<256,SUBNET>>>;
template <typename SUBNET> using alevel2 = ares<128,ares<128,ares_down<128,SUBNET>>>;
template <typename SUBNET> using alevel3 = ares<64,ares<64,ares<64,ares_down<64,SUBNET>>>>;
template <typename SUBNET> using alevel4 = ares<32,ares<32,ares<32,SUBNET>>>;

using anet_type = loss_metric<fc_no_bias<128,avg_pool_everything<
        alevel0<
                alevel1<
                        alevel2<
                                alevel3<
                                        alevel4<
                                                max_pool<3,3,2,2,relu<affine<con<32,7,7,2,2,
                                                        input_rgb_image_sized<150>
                                                >>>>>>>>>>>>;

shape_predictor sp;
anet_type net;

struct myPoint {
    int x;
    int y;
};

typedef struct myPoint Point;

std::string jstring2string(JNIEnv *env, jstring jStr) {
    if (!jStr)
        return "";

    std::vector<char> charsCode;
    const jchar *chars = env->GetStringChars(jStr, NULL);
    jsize len = env->GetStringLength(jStr);
    jsize i;

    for( i=0;i<len; i++){
        int code = (int)chars[i];
        charsCode.push_back( code );
    }
    env->ReleaseStringChars(jStr, chars);

    return  std::string(charsCode.begin(), charsCode.end());
}

std::vector<long> get_points_center(full_object_detection shape, int start, int end)
{
    double x, y;
    x = y = 0;
    for (int i = start; i <= end; i++) {
        x += shape.part(i).x();
        y += shape.part(i).y();
    }
    double num = end - start + 1;

    std::vector<long> result;
    result.push_back(x / num);
    result.push_back(y / num);

    return result;
}

array2d<rgb_pixel> convertBitmap2array2d(jintArray image_data, int width, int height, JNIEnv *env)
{
    std::vector<int>image_datacpp(height*width);

    jsize len = env->GetArrayLength(image_data);
    jint *body = env->GetIntArrayElements(image_data, 0);

    for (jsize i=0;i<len;i++){
        image_datacpp[i]=(int)body[i];
    }

    array2d<rgb_pixel> image;

    image.set_size(width, height);

    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            int clr = image_datacpp[i*width+j];
            unsigned char red = (clr & 0x00ff0000) >> 16;
            unsigned char green = (clr & 0x0000ff00) >> 8;
            unsigned char blue = clr & 0x000000ff;
            dlib::rgb_pixel pt(red,green,blue);
            image[i][j]=pt;
        }
    }

    return image;
}

extern "C" JNIEXPORT void JNICALL
Java_psu_ttx_fzoperation_FaceRecognizer_initFromJNI(JNIEnv *env, jobject thiz)
{
    deserialize("/sdcard/shape_predictor.dat") >> sp;
    // deserialize("/sdcard/dlib_face_recognition_resnet_model_v1.dat") >> net;
}

extern "C" JNIEXPORT jobjectArray JNICALL
Java_psu_ttx_fzoperation_FaceRecognizer_alignFaceFromJNI(JNIEnv *env, jobject thiz,
                                                jintArray image_data, jint image_width,jint image_height)
{
    int width = image_width;
    int height = image_height;

    array2d<rgb_pixel> image = convertBitmap2array2d(image_data, width, height, env);
//    frontal_face_detector detector = get_frontal_face_detector();
//    std::vector<dlib::rectangle> dets= detector(image);
    std::vector<full_object_detection> shapes;
    rectangle face = rectangle(0, 0, width, height);
    full_object_detection shape = sp(image, face);

    Point leftEyePoint, rightEyePoint, nosePoint, leftMouthPoint, rightMouthPoint;
    for (unsigned long j = 0; j < 1/*dets.size()*/; ++j)
    {
        full_object_detection shape = sp(image, face);
        // cout << "number of parts: "<< shape.num_parts() << endl;
        if (shape.num_parts() == 0) {
            return nullptr;
        }

        // Left eye index 37 ~ 42
        std::vector<long> leftEyePosition = get_points_center(shape, 36, 41);
        cout << "Left eye x: " << leftEyePosition[0] << ", y: " << leftEyePosition[1] << endl;
        leftEyePoint.x = leftEyePosition[0];
        leftEyePoint.y = leftEyePosition[1];

        // Right eye index 43 ~ 48
        std::vector<long> rightEyePosition = get_points_center(shape, 42, 47);
        // cout << "Right eye x: " << rightEyePosition[0] << ", y: " << rightEyePosition[1] << endl;
        rightEyePoint.x = rightEyePosition[0];
        rightEyePoint.y = rightEyePosition[1];

        // Left mouth point 49
        std::vector<long> leftMouthPosition = get_points_center(shape, 48, 48);
        // cout << "Left mouth x: " << leftMouthPosition[0] << ", y: " << leftMouthPosition[1] << endl;
        leftMouthPoint.x = leftMouthPosition[0];
        leftMouthPoint.y = leftMouthPosition[1];

        // Right mouth point 55
        std::vector<long> rightMouthPosition = get_points_center(shape, 54, 54);
        // cout << "Right mouth x: " << rightMouthPosition[0] << ", y: " << rightMouthPosition[1] << endl;
        rightMouthPoint.x = rightMouthPosition[0];
        rightMouthPoint.y = rightMouthPosition[1];

        // Nose 31
        std::vector<long> nosePosition = get_points_center(shape, 30, 30);
        // cout << "Nose x: " << nosePosition[0] << ", y: " << nosePosition[1] << endl;
        nosePoint.x = nosePosition[0];
        nosePoint.y = nosePosition[1];
    }

    double k = double(leftEyePoint.y - rightEyePoint.y) / double(leftEyePoint.x - rightEyePoint.x);
    double angle = atan(k) * 180 / 3.1415926;
    Point midEyePoint, midMouthPoint;
    midEyePoint.x = (leftEyePoint.x + rightEyePoint.x) / 2;
    midEyePoint.y = (leftEyePoint.y + rightEyePoint.y) / 2;
    midMouthPoint.x = (leftMouthPoint.x + rightMouthPoint.x) / 2;
    midMouthPoint.y = (leftMouthPoint.y + rightMouthPoint.y) / 2;

    double distance = sqrt(pow(midEyePoint.x - midMouthPoint.x, 2)
                           + pow(midEyePoint.y - midMouthPoint.y, 2));
    double scaleFactor = 48.0 / distance;

    jclass classDouble = (*env).FindClass("java/lang/Double");
    jobjectArray outJNIArray = (*env).NewObjectArray(6, classDouble, NULL);

    jmethodID midDoubleInit = (*env).GetMethodID(classDouble, "<init>", "(D)V");
    if (NULL == midDoubleInit) return NULL;
    jobject objScaleFactor = (*env).NewObject(classDouble, midDoubleInit, scaleFactor);
    jobject objAngle = (*env).NewObject(classDouble, midDoubleInit, angle);
    jobject objCenterX = (*env).NewObject(classDouble, midDoubleInit, (double) leftEyePoint.x);
    jobject objCenterY = (*env).NewObject(classDouble, midDoubleInit, (double) leftEyePoint.y);
    jobject objEyeDistance = (*env).NewObject(classDouble, midDoubleInit, sqrt(pow(leftEyePoint.x - rightEyePoint.x, 2)
                                                                               + pow(leftEyePoint.y - rightEyePoint.y, 2)));
    jobject objEyeMouthDistance = (*env).NewObject(classDouble, midDoubleInit,  distance);

    // Set to the jobjectArray
    (*env).SetObjectArrayElement(outJNIArray, 0, objScaleFactor);
    (*env).SetObjectArrayElement(outJNIArray, 1, objAngle);
    (*env).SetObjectArrayElement(outJNIArray, 2, objCenterX);
    (*env).SetObjectArrayElement(outJNIArray, 3, objCenterY);
    (*env).SetObjectArrayElement(outJNIArray, 4, objEyeDistance);
    (*env).SetObjectArrayElement(outJNIArray, 5, objEyeMouthDistance);

    return outJNIArray;
}

//extern "C" JNIEXPORT void JNICALL
//Java_psu_ttx_fzrecog_MainActivity_fzRegResnet(JNIEnv *env, jobject thiz,
//                                              jintArray image_data, jint image_width,jint image_height)
//{
//    frontal_face_detector detector = get_frontal_face_detector();
//
//    int width = image_width;
//    int height = image_height;
//    array2d<rgb_pixel> image = convertBitmap2array2d(image_data, width, height, env);
//    matrix<rgb_pixel> img = mat(image);
//
//    std::vector<dlib::rectangle> dets= detector(image);
//    std::vector<matrix<rgb_pixel>> faces;
//    for (unsigned long j = 0; j < dets.size(); ++j)
//    {
//        full_object_detection shape = sp(image, dets[j]);
//        matrix<rgb_pixel> face_chip;
//        extract_image_chip(img, get_face_chip_details(shape,150,0.25), face_chip);
//        faces.push_back(move(face_chip));
//    }
//
//    auto face_descriptors = net(faces);
//}